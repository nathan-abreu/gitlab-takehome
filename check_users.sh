#!/bin/bash

# Process MD5 Checksum of the Current User List
NEW_MD5=$(./user_list.sh | md5sum | cut -d' ' -f1)

# Check if MD5 file exists and compare values
if [ -e /var/log/current_users ]; then
    OLD_MD5=$(cat /var/log/current_users)
    if [ "$OLD_MD5" != "$NEW_MD5" ]; then
        echo "$(date "+%Y-%m-%d %H:%M:%S") changes occurred" >> /var/log/user_changes
    fi
fi

# Write new MD5 value on file current_users
echo "$NEW_MD5" > /var/log/current_users