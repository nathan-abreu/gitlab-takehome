# Senior Support Engineer Take-home Assessment 

## Question 1: Write a Ruby or Bash script that will print usernames of all users on a Linux system together with their home directories. Here's some example output: 

```bash
gitlab:/home/gitlab
nobody:/nonexistent
```

Each line is a concatenation of a username, the colon character (:), and the home directory path for that username. Your script should output such a line for each user on the system.

Next, write a second script that:

● Takes the full output of your first script and converts it to an MD5 hash. 

● On its first run stores the MD5 checksum into the /var/log/current_users file. 

● On subsequent runs, if the MD5 checksum changes, the script should add a line in the /var/ log/user_changes file with the message, DATE TIME changes occurred, replacing > DATE and TIME with appropriate values, and replaces the old MD5 checksum in /var/log/ current_users file with the new MD5 checksum. 

### Answer:

**First Script**:

```bash
#!/bin/bash
awk -F: '{print $1":"$6}' /etc/passwd
```

**Second Script**:

```bash
#!/bin/bash

# Process MD5 Checksum of the Current User List
NEW_MD5=$(./user_list.sh | md5sum | cut -d' ' -f1)

# Check if MD5 file exists and compare values
if [ -e /var/log/current_users ]; then
    OLD_MD5=$(cat /var/log/current_users)
    if [ "$OLD_MD5" != "$NEW_MD5" ]; then
        echo "$(date "+%Y-%m-%d %H:%M:%S") changes occurred" >> /var/log/user_changes
    fi
fi

# Write new MD5 value on file current_users
echo "$NEW_MD5" > /var/log/current_users

```

**Crontab Entry:**

```bash
0 * * * * /usr/local/bin/check_users.sh
```

**Links for the scripts in the repository**

[user_list.sh][def]

[check_users.sh][def2]

[crontab.txt][def3]

---

## Question 2: A user is complaining that it's taking a long time to load a page on our web application. In your own words, write down and discuss the possible cause(s) of the slowness. Also, describe how you would begin to troubleshoot this issue.

Keep the following information about the environment in mind: 

● The web application is written in a modern MVC web framework. 

● Application data is stored in a relational database. 

● All components (web application, web server, database) are running on a single Linux box with 8GB RAM, 2 CPU cores, and SSD storage with ample free space. 

● You have root access to this Linux box. 

We are interested in learning about your experience with modern web applications, and your ability to reason about system design and architectural trade-offs. There are no right and wrong answers to this question. Feel free to write as much or as little as you feel is necessary. 

### Answer:

Dear Client,

I hope this message finds you well. I've been closely looking into the reported speed issues with our web application page. Let me break down the possible causes and our troubleshooting plan and ask you some questions.

Does the slowness occur on all pages or just one specific page?

When did you first notice the slowness?

Are other users reporting similar problems?


**Potential Causes of Slowness:**

**Database Bottlenecks:**  The relational database model we use may experience latency due to factors like unoptimized SQL queries, connections pulling and multiplexing, insufficient indexing, or table locking. Evaluating slow query logs and optimizing database schemas can be of great value on this case. Also we can take a look on the following:

CPU Saturation: With both the web server and the database running on a 2-core CPU, it's possible for the system to be CPU-bound, especially if there's a surge in user requests or data processing.

Memory Limitations: 8GB of RAM can be exhausted if there are many simultaneous connections, large datasets being processed, or memory leaks in the application.

We can check if the number of worker processes or threads are appropriately configured. A misconfigured web server could be a bottleneck.

**I/O Wait:** Even with an SSD, if there are many concurrent read-write operations, I/O wait times can increase, slowing down response times.

**Web Server Configuration:**  Incorrectly tuned web server settings, such as suboptimal timeout values or improperly sized connection pools, can negatively impact user experience. An audit of our web server's configuration against best practices will shed light on any misconfigurations.

**Network Latency and Bandwidth:** The underlying network infrastructure plays a pivotal role. Bandwidth limitations, latency due to geographical distances, or even packet loss can hamper speed. Tools like traceroute, ping, and mtr will assist in mapping out network performance.

**Third-Party Dependencies:**  Integration with external services or APIs introduces another variable on this matter. Delays or inefficiencies in these services can affect the application's responsiveness. Monitoring response times and failover strategies for these services are very important in this case.

**Our Troubleshooting Plan:**

**Application Logs:** Will Review application logs for any error messages or slow SQL queries.
See if there are any repeating patterns, or if specific queries or operations are taking longer than they should.

**Server Monitoring:** We'll use tools like *htop* and *vmstat* to peek into the server's workload. This helps us see if our server's resources are stretched thin.

**Database Health Check:** Using commands like *SHOW FULL PROCESSLIST*; we can see if there are any database queries taking longer than they should. We'll also ensure that our database tables are properly indexed to speed up data fetch times. Also we can use EXPLAIN on slow queries to understand how they’re being executed and if they can be optimized.

**Application Audit:** We'll dig into our application logs to identify any slow processes or problematic areas. The MVC framework we use has built-in tools to help us pinpoint these.

**External Service Review:** For our third-party services, we’ll run tests to check their response times. If there are any hiccups, we'll consider alternative approaches or work with those providers to improve speeds.

We’re on top of this, and our aim is to ensure the application runs smoothly and efficiently for you. Please bear with us as we work through this, and I promise to keep you updated on our progress. Your feedback has been invaluable, and if you have any further questions or concerns, feel free to reach out.

Best regards,

Nathan Abreu


**Answers were tailored using the sources bellow:**

[Source: MySQL's official documentation about optimization and the slow query log.][def5]

[Source: Microsoft's guidelines on performance patterns and anti-patterns for ASP.NET (which is an MVC framework).][def6]

[Source: NGINX's documentation on optimizing for performance.][def7]


## Question 3:  Study the Git commit graph shown below. What sequence of Git commands could have resulted in this commit graph?

![commitgraph](gitlabgraph_takehome.png) 

### Answer:
```bash
#Step 1
git add .
git commit -m "first commit"

#Step 2
git add .
git commit -m "second commit"

#Step 3
git checkout -b feature-branch
git add .
git commit -m "awesome feature"
git checkout main

# Step 4 
git add .
git commit -m "third commit"

# Step 5 
git merge feature-branch -m "merge to main"

# Step 6 
git add .
git commit 

```
---

## Question 4: GitLab has hired you to write a Git tutorial for beginners on: Using Git to implement a new feature/change without affecting the main branch In your own words, write a tutorial/blog explaining things in a beginner-friendly way. Make sure to address both the "why" and "how" for each Git command you use. Assume the audience are readers of a well-known blog. 

# **Implementing New Features in Git Without Disrupting the Main Code: A Friendly Guide**

Hello readers! Today, we're going to delve into the realm of Git, and understand how to safely experiment with our projects without any hiccups. Let’s get started!

## **Why Git?**
Git is a version control system. It allows multiple people to work on the same project without stepping on each other's toes. Imagine a group of artists painting on the same canvas, but each with their own section. Git ensures they don’t accidentally paint over each other’s masterpiece.

## **Branching: A Quick Overview :deciduous_tree:**
When you're working on a project and want to add something new or test out an idea, it's a great practice to do this work separately, away from your main project. In Git, this separate workspace is called a 'branch'.

*Git branching allows developers to create multiple lines of development within a single repository. Think of it as multiple pathways in a project where each branch represents a separate environment. This enables simultaneous development of different features or testing, all while ensuring the main code remains stable.*

![Diagram showing Git branching](https://wac-cdn.atlassian.com/dam/jcr:a905ddfd-973a-452a-a4ae-f1dd65430027/01%20Git%20branch.svg?cdnVersion=1170)

## **Why Use Branches?**
- **Isolation:** When you create a branch, you’re essentially creating a unique environment where you can make changes without affecting the main code.
- **Flexibility:** If your new idea doesn't work out, you can simply delete or abandon the branch without any harm done.
- **Collaboration:** Allows multiple developers to work on different features simultaneously without interfering with each other.

Alright, now let's get into the "how" of it.

## **Setting The Stage**

Firstly, make sure you have Git installed. If not, [download it here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).

### **1. Access Your Project:**
Navigate to your project using the command line:
```bash
# Can be a folder stored anywhere in you pc or laptop
cd your_project_directory 

```

### **2. Initialize Git:**
If you've never set up Git for your project:
```bash
git init
```
This initializes a new Git repository and starts tracking an existing directory.

### **3. Check Your Progress**
```bash
git status
```
***git status*** lets you see what changes you've made. Think of it as a summary page.

### **4. Save Your Current Progress:**
Before branching, it’s a good idea to make sure all your changes are saved:
```bash
git add .
git commit -m "Describe what you've done here"
```
***git add*** stages changes, and ***git commit*** saves them with a note to help you remember what you did.


## **Creating a New Branch**

### **5. Create and Switch to Your Branch:**
```bash
git checkout -b branch_name
```
This command creates a new branch and switches you over to it.

### **6. Work on Your New Feature:**
With your new branch ready, you can start making changes.

### **7. Keep Tracking Your Work:**
```bash
git status
git add .
git commit -m "Details about the new changes"
```
### **Integrating Your Branch Back into the Main Code**
Once you're satisfied with your new feature:

## **8. Return to the Main Branch:**
```bash
git checkout main
```

## **9. Bring in Your New Changes:**
```bash
git merge branch_name
```
This command integrates the changes from your branch back into the main code.

## **10. Celebrate! 🎉**
You've just added a new feature without risking the stability of your main project. Well done!


---

**Source**: [Official Git Documentation](https://git-scm.com/doc)

For a deep dive and more technical explanations, readers are encouraged to explore the official documentation provided by the creators of Git.


## Question 5: What is a technical book/blog you read recently that you enjoyed? Please include a brief review of what you especially liked or didn’t like about it. 

### Answer:

## Lately i'm reading the [KodeKloud Blog](https://kodekloud.com/blog/)

**Things I Really Liked:**

**It's Super Informative:** They really go into detail about tech stuff, but in a way that makes sense even if you're not a total tech nerd.

**Easy to Understand:** They use plain English. No jargon overload. You know how some tech blogs can be like reading a foreign language? this one is more simple and you can get a lot of from subjects you don't master.

**Hands-On Tips:** They've got cool hands-on examples. So, if you're trying to figure something out, they show you how to do it step by step.

**Lots of Links to Other Cool Stuff:** You can jump from one article to another. Great if you're down a learning rabbit hole.

**They Get me Involved:** They've got this thing called ***"KodeKloud Engineer"*** where you can try stuff out in a real setting. this is pretty nice.

**in the other hand there are some things They Could Work On:**

**Needs More Pictures:** It's a bit text-heavy. A few diagrams or videos would make it pop more.

**Easier to Find Stuff:** They could organize things a bit better. Like, if you're a beginner, where should you start? That kinda thing.
they do that but not for all articles and not for the ones i'm reading :-)

**A Place to Chat:** I did not see a place where readers can chat or ask questions in the blog and not in a slack channel, That would be a cool add.

**More External Links:** It's always good to have links to other places for more info and that would show they are concerned with the content itself and not only sell courses.

[def]: https://gitlab.com/nathan-abreu/gitlab-takehome/-/blob/main/user_list.sh
[def2]: https://gitlab.com/nathan-abreu/gitlab-takehome/-/blob/main/check_users.sh
[def3]: https://gitlab.com/nathan-abreu/gitlab-takehome/-/blob/main/crontab.txt
[def5]: https://dev.mysql.com/doc/refman/8.0/en/optimization.html
[def6]: https://docs.microsoft.com/en-us/previous-versions/msp-n-p/ff647787(v=pandp.10)
[def7]: https://www.nginx.com/blog/tuning-nginx/
